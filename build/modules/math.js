"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.factorial = exports.power = void 0;
function add() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    var result = 0;
    args.forEach(function (o) { return result += o; });
    return result;
}
function factorial(n) {
    if (n === 1) {
        return 1;
    }
    return n * factorial(n - 1);
}
exports.factorial = factorial;
function power(x, y) {
    if (y === 1) {
        return x;
    }
    return x * power(x, y - 1);
}
exports.power = power;
exports.default = add;
