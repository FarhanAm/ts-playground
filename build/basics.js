"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var message = "Hello World!";
console.log(message);
var color = "Red";
// this will throw an error in both js in runtime and ts
// because color is not callable!
// color();
var car = {
    name: "Corvette",
    color: "black",
};
console.log(car.name);
// this is valid js and return undefined in run time
// but in ts this is an error
// console.log(car.maxSpeed);
// message.toLocaleLowercase();
var a = "a";
var b = "b";
// This condition will always return 'true' since the types '"a"' and '"b"' have no overlap.
// if (a !== "b") {
//     console.log(a);
// } else if (a === "a") {
//     // do sth else
// }
// Explicit Types
function helloUser(name, day) {
    console.log("Hello " + name + ", today is " + day);
}
// helloUser("Farhan", 5);
helloUser("Farhan", "Thursday");
// firstName is a string and we cant assign a number to it
var firstName = "Farhan";
// firstName = 9;
console.log(firstName);
var person = {
    name: "Farhan",
    age: 22
};
var obj1 = {
    x: 1
};
console.log(obj1);
// the type of a variable is inferred based on the type of its initializer
var obj2 = {
    name: "user1"
};
console.log(typeof (obj2));
// specifying the types of both input and output 
function square(x) {
    return x * x;
}
console.log(square(5));
// logs number
console.log(typeof (square(4)));
// myArr accepts elements of type number
var myArr = [];
// myArr.push("first");
console.log(myArr);
// optional properties
function multiply(a, b) {
    if (b) {
        return a * b;
    }
    return a;
}
console.log(multiply(1));
console.log(multiply(2, 6));
// union types
function getAgeOrName(input) {
    if (typeof (input) === "string") {
        return ("My name is " + input);
    }
    else {
        return ("I am " + input + " years old");
    }
}
console.log(getAgeOrName("Farzan"));
console.log(getAgeOrName(28));
function printInput(str) {
    // handling different inputs
    if (typeof (str) === "string") {
        console.log(str.toUpperCase());
    }
    else if (typeof (str) === "number") {
        console.log(str + 10);
    }
}
printInput("devEloper");
printInput(23);
function getRectangleArea(rectangle) {
    return rectangle.latitud * rectangle.longitude;
}
console.log(getRectangleArea({ longitude: 6, latitud: 2 }));
var car1 = "Veneno";
console.log(typeof (car1), car1);
function circleArea(circle) {
    return Math.floor(Math.pow(circle.radius, 2) * Math.PI);
}
// console.log(circleArea({radius: 3}));
console.log(circleArea({ radius: 3, pi: 3 }));
var animal = "Tiger";
var animal2 = "Panda";
console.log(typeof (animal), typeof (animal2));
// literal types
var x = "x";
// x = "y";
// type assertion
x = "z";
console.log(x);
function getDirection(direction) {
    console.log("go to the " + direction.toUpperCase());
}
getDirection("left");
// getDirection("straight");
// ***********************************************
// If you do not export anything from a TypeScript file, 
// it gets compiled in the global scope. To make a .ts file 
// an enclosed module you need to export at least one thing.
exports.default = getDirection;
