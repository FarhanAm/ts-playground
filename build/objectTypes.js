"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
Object.defineProperty(exports, "__esModule", { value: true });
function getId(student) {
    console.log("student name: " + student.name);
    console.log("student id: " + student.id);
    student.age && console.log("student age: " + student.age);
}
getId({ name: "Farhan", id: "12qa" });
;
function petAction(_a) {
    var _b = _a.kind, kind = _b === void 0 ? "Dog" : _b, _c = _a.sound, sound = _c === void 0 ? "woof!" : _c;
    console.log(kind + " says " + sound);
}
petAction({ kind: "Cat", sound: "meow!" });
petAction({});
function setValue(setVal) {
    console.log("a was " + setVal.a);
    setVal.a = setVal.b + setVal.a;
    console.log("a is " + setVal.a);
}
setValue({ a: 2, b: 3 });
function setValue2(setVal) {
    console.log("a was " + setVal.a);
    // property a is readonly so we cant reasign it
    // setVal.a = setVal.b + setVal.a;
    console.log("a is still " + setVal.a);
}
setValue2({ a: 2, b: 3 });
function assign(pushArr) {
    var _a;
    console.log(pushArr.arr);
    // ***** property arr is readonly but its internal 
    // ***** contents can be changed
    (_a = pushArr.arr).push.apply(_a, pushArr.pushData);
    console.log(pushArr.arr);
}
assign({ arr: [1, 2], pushData: [3] });
function assign2(_a) {
    var arr = _a.arr, pushData = _a.pushData, pushData2 = _a.pushData2;
    console.log(arr);
    arr.push.apply(arr, __spreadArray(__spreadArray([], pushData), pushData2));
    console.log(arr);
}
assign2({ arr: [1], pushData: [2, 3], pushData2: ["Hello!"] });
;
;
var myCar = {
    model: "Sian",
    color: "Sunrise Red",
};
console.log("I hava a(n) " + myCar.color + " " + myCar.model + ".");
var coin1 = { rankOrSymbol: "BTC" };
var coin2 = { rankOrSymbol: 1 };
if (typeof (coin1.rankOrSymbol) === "string") {
    console.log(coin2);
}
console.log(coin1.rankOrSymbol.toLowerCase());
// Array type
var myArr = [1, 2, 3];
// underneath array definition is similar to the above one
var myArr2 = [1, 2, 3];
// myArr.push(...["a"]);
myArr.push.apply(myArr, [12]);
console.log(myArr);
var myTup = ["a", 1, 2, 3, "z"];
console.log(typeof (myTup), myTup);
