"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// truthiness narrowing
function printNames(name) {
    if (name && typeof (name) === "object") {
        for (var _i = 0, name_1 = name; _i < name_1.length; _i++) {
            var i = name_1[_i];
            console.log(i);
        }
    }
    else if (typeof (name) === "string") {
        console.log(name);
    }
}
printNames(["john", "mike"]);
// equality narrowing
function forFun(x, y) {
    if (x === y) {
        console.log(Math.pow(x, y));
    }
    else {
        console.log(x);
        console.log(y);
    }
}
forFun(3, 3);
forFun("true", 3);
function carType(cars) {
    switch (cars.model) {
        case "Aventador":
            return cars.model + " is a Super Sport";
        case "Urus":
            return cars.model + " is a SUV";
        case "Phantom":
            return cars.model + " is a Sedan";
    }
}
// this will return undefined
// console.log(carType({ model: "Veneno" }));
console.log(carType({ model: "Phantom" }));
function getNewDate(dt) {
    if (dt.date instanceof Date) {
        console.log(dt.date.getFullYear());
    }
    else {
        console.log(dt.date.toUpperCase());
    }
}
getNewDate({ date: Date() });
getNewDate({ date: new Date() });
// assignment
var randNum = Math.floor(Math.random() * 100);
var oddOrEven = randNum % 2 === 0 ? randNum / 2 : "odd";
console.log(oddOrEven);
function isCat(animal) {
    return typeof animal.numberOfLives === "number";
}
var animal3 = {
    numberOfLives: 9
};
if (isCat(animal3)) {
    console.log("animal3 is a Cat");
}
else {
    console.log("animal3 is a Dog");
}
function getArea(shape) {
    switch (shape.kind) {
        case "Circle":
            return Math.pow(shape.radius, 2) * 3;
        case "Square":
            return shape.side * shape.side;
        default:
            var _exhastiveCheck = shape;
            return _exhastiveCheck;
    }
}
console.log(getArea({ kind: "Circle", radius: 3 }));
// ***********************************************
// If you do not export anything from a TypeScript file, 
// it gets compiled in the global scope. To make a .ts file 
// an enclosed module you need to export at least one thing.
exports.default = getArea;
