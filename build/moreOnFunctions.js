"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// function type expression
function helloUser(fn) {
    fn("Farhan");
}
function printMessage(str) {
    console.log("hello " + str);
}
helloUser(printMessage);
function logicOr(fn) {
    return fn(true, false);
}
function orOp(a, b) {
    return a || b;
}
console.log(logicOr(orOp));
function printStr(fn) {
    console.log("this is a " + fn.type.toUpperCase() + " and the output is " + fn(6));
}
function numToStr(a) {
    return String(a);
}
numToStr.type = "function property";
printStr(numToStr);
var MadeFromString = /** @class */ (function () {
    function MadeFromString(name) {
        this.name = name;
        console.log("ctor invoked");
    }
    return MadeFromString;
}());
function makeObj(n) {
    return new n("hello!");
}
console.log(makeObj(MadeFromString).name);
// generic functions
function printArrItems(arr) {
    arr.map(function (item) { return console.log(item); });
}
printArrItems([2, 3, 5, 7]);
function filter(arr, fn) {
    return arr.filter(fn);
}
var evenNums = filter([1, 2, 3, 4, 5, 6, 7, 8,], function (n) { return n % 2 === 0; });
console.log(evenNums);
// Constraints
function getMaxLen(a, b, c) {
    var maxLen;
    if (a.length > b.length) {
        maxLen = a;
    }
    else {
        maxLen = b;
    }
    if (c.length > maxLen.length) {
        maxLen = c;
    }
    return maxLen;
}
// in this function arguments with equal length arent handled properly;
console.log(getMaxLen("a", "asdd", "asdfrwg"));
console.log(getMaxLen([1, 2, 3], [1], [4]));
////////////////
function filter1(arr, fn) {
    return arr.filter(fn);
}
function filter2(arr, fn) {
    return arr.filter(fn);
}
// optional parameter
function multipleTwo(n) {
    if (n) {
        console.log(n * 2);
    }
    else {
        console.log("multiplier is 2");
    }
}
multipleTwo(2);
multipleTwo();
function add(a, b, c) {
    if (c) {
        return a + b + c;
    }
    return a + b;
}
console.log(add(1, 2, 3));
console.log(add("farhan", " aqaei"));
// this key word in function
var greetUser = {
    name: "Admin",
    greet: function () {
        console.log("Hello " + this.name);
    }
};
greetUser.greet();
// rest parameters
function addParams() {
    var params = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        params[_i] = arguments[_i];
    }
    var result = 0;
    for (var _a = 0, params_1 = params; _a < params_1.length; _a++) {
        var i = params_1[_a];
        result += i;
    }
    return result;
}
console.log(addParams(2, 4, 5));
// rest arguments
var a1 = [1, 3, 4];
var a2 = [5, 6];
a1.push.apply(a1, a2);
console.log(a1);
// ***********************************************
// If you do not export anything from a TypeScript file, 
// it gets compiled in the global scope. To make a .ts file 
// an enclosed module you need to export at least one thing.
exports.default = addParams;
