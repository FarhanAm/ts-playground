"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getId(arg) {
    return arg;
}
// two ways to call a generic function
console.log(getId("waqds"));
console.log(getId(4323));
function getId2(arg) {
    return arg;
}
var myId = getId2;
// console.log(myId(234));
console.log(myId("wd32"));
// Generic class
var GenericAdd = /** @class */ (function () {
    function GenericAdd() {
    }
    return GenericAdd;
}());
var myAdd = new GenericAdd();
myAdd.initVal = 2;
myAdd.add = function (a, b) {
    return a + b;
};
console.log(myAdd.add(myAdd.initVal, 10));
function returnLen(arg) {
    console.log(arg, arg.length);
    return arg;
}
returnLen("Farhan");
// Type parameter in generic constraints
function getEl(obj, key) {
    return obj[key];
}
;
console.log(getEl({ a: 1, b: 2 }, "b"));
var myObj = "age";
// ---------- Typeof
function returnNum(n) {
    return n;
}
var cars = [
    { model: "Veneno", isSUV: false },
    { model: "Urus", isSUV: false }
];
;
var userId = { numId: 1234 };
;
