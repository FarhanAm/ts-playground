"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// "strictPropertyInitialization" is set to false in tsconfig
// if we set this to true we 'must' provide initializer for class fields
// or use the definite assignment assertion operator, !
var AnimalWithoutCtor = /** @class */ (function () {
    function AnimalWithoutCtor() {
    }
    return AnimalWithoutCtor;
}());
var cat = new AnimalWithoutCtor;
cat.species = "mammal";
cat.hasFur = true;
console.log(cat);
// field initializer and infering type
var CarWithoutCtor = /** @class */ (function () {
    function CarWithoutCtor() {
        this.name = "Veneno";
        this.maxSpeed = 355;
    }
    return CarWithoutCtor;
}());
var defaultCar = new CarWithoutCtor;
console.log(defaultCar, typeof defaultCar.name);
// constructor and readonly fields
// *** the field needs to be initialized in the constructor itself ***
var Animal = /** @class */ (function () {
    function Animal(species) {
        if (species === void 0) { species = "insects"; }
        this.species = species;
    }
    return Animal;
}());
var blueJ = new Animal("bird");
// cant change isAlive because it is a readonly field
// blueJ.isAlive = false;
console.log(blueJ.species);
// super calls and methods and Getter/Setter
var Dog = /** @class */ (function (_super) {
    __extends(Dog, _super);
    function Dog() {
        var _this = _super.call(this) || this;
        _this.species = "mammal";
        return _this;
    }
    Dog.prototype.isExtinct = function () {
        return this.extinct ?
            "there are not any " + this.type + "s any more!" :
            "there are still " + this.type + "s!";
    };
    Object.defineProperty(Dog.prototype, "get_species", {
        get: function () {
            return this.species;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Dog.prototype, "set_type", {
        set: function (t) {
            this.type = t;
        },
        enumerable: false,
        configurable: true
    });
    return Dog;
}(Animal));
var scooby = new Dog;
console.log(scooby);
scooby.set_type = "dog";
console.log(scooby);
console.log(scooby.isExtinct());
// implements
var Dolphin = /** @class */ (function () {
    function Dolphin() {
        this.species = "mammal";
        this.extinct = false;
        this.type = "Fish";
    }
    return Dolphin;
}());
var dolphin = new Dolphin;
console.log(dolphin);
// extend - method overridong - member visibility - static members
var Car = /** @class */ (function () {
    function Car(company, model, maxSpeed, isHyperCar, engineId) {
        if (isHyperCar === void 0) { isHyperCar = false; }
        this.company = company;
        this.model = model;
        this.maxSpeedKph = maxSpeed;
        this.isHyperCar = isHyperCar;
        this.engineId = engineId;
    }
    Car.prototype.maxSpeedToMph = function () {
        return this.maxSpeedKph * .6;
    };
    return Car;
}());
var BMW = /** @class */ (function (_super) {
    __extends(BMW, _super);
    function BMW() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    BMW.prototype.maxSpeedToMph = function () {
        return this.maxSpeedKph * .6 + " mph";
    };
    Object.defineProperty(BMW.prototype, "get_engineId", {
        get: function () {
            return this.engineId;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(BMW.prototype, "set_color", {
        set: function (color) {
            this.color = color;
        },
        enumerable: false,
        configurable: true
    });
    BMW.vehicle = "Car";
    return BMW;
}(Car));
// vehicle is a static member so to access it we dont need to instanciate BMW class
var vehicle1 = BMW.vehicle;
var i8 = new BMW("BMW", "i8", 250, true, "fd12");
console.log(i8);
console.log(i8.maxSpeedToMph());
i8.set_color = "white-blue";
console.log(i8);
// we dont have direct access to protected field "engineId"
console.log(i8.get_engineId);
var getLen = /** @class */ (function () {
    function getLen(a) {
        var _this = this;
        this.getLen = function () {
            return _this.a.length;
        };
        this.a = a;
    }
    return getLen;
}());
var arr = new getLen([1, 2, 3]);
console.log(arr.getLen());
// class expressions
var Lamborghini = /** @class */ (function (_super) {
    __extends(class_1, _super);
    function class_1(company, model, maxSpeed, isHyperCar, engineId) {
        if (isHyperCar === void 0) { isHyperCar = false; }
        var _this = _super.call(this, company, model, maxSpeed, isHyperCar, engineId) || this;
        _this.getCarName = function () {
            return _this.model;
        };
        return _this;
    }
    return class_1;
}(Car));
var aventador = new Lamborghini("Lamborghini", "Aventador", 350, true, "ad34");
console.log(aventador);
console.log(aventador.maxSpeedToMph());
// abstract class
var Person = /** @class */ (function () {
    function Person() {
    }
    return Person;
}());
// Cannot create an instance of an abstract class
// const person1 = new Person;
var Employee = /** @class */ (function (_super) {
    __extends(Employee, _super);
    function Employee(name, age, gender) {
        var _this = _super.call(this) || this;
        _this.name = name;
        _this.age = age;
        _this.gender = gender;
        return _this;
    }
    return Employee;
}(Person));
var person1 = new Employee("John", 22, "Male");
console.log(person1);
// variable with the type of class "Car" that instanciate class "Lamborghini"
var car1 = new Lamborghini("Lamborghini", "Gallardo", 325, true, "hg345");
console.log(car1);
console.log(car1.maxSpeedToMph());
