// "strictPropertyInitialization" is set to false in tsconfig
// if we set this to true we 'must' provide initializer for class fields
// or use the definite assignment assertion operator, !
class AnimalWithoutCtor {
    species!: string;
    hasFur: boolean;
}
const cat = new AnimalWithoutCtor;
cat.species = "mammal";
cat.hasFur = true;

console.log(cat);

// field initializer and infering type
class CarWithoutCtor {
    name = "Veneno";
    maxSpeed : number = 355;
}
const defaultCar = new CarWithoutCtor;
console.log(defaultCar, typeof defaultCar.name);

// constructor and readonly fields
// *** the field needs to be initialized in the constructor itself ***
class Animal {
    species: string;
    type?: string;
    readonly extinct: false;
    constructor (species = "insects") {
        this.species = species;
    }
}
const blueJ = new Animal("bird");
// cant change isAlive because it is a readonly field
// blueJ.isAlive = false;
console.log(blueJ.species);

// super calls and methods and Getter/Setter
class Dog extends Animal {
    constructor () {
        super();
        this.species = "mammal";
    }
    isExtinct (): string {
        return this.extinct ?
        `there are not any ${this.type}s any more!` : 
        `there are still ${this.type}s!`
    }
    get get_species () {
        return this.species;
    }
    set set_type (t: string) {
        this.type = t;
    }
}
const scooby = new Dog;
console.log(scooby);
scooby.set_type = "dog";
console.log(scooby);
console.log(scooby.isExtinct());

// implements
class Dolphin implements Animal {
    species = "mammal";
    readonly extinct = false;
    type = "Fish";
}
const dolphin = new Dolphin;
console.log(dolphin);

// extend - method overridong - member visibility - static members
class Car {
    company: string;
    model: string;
    maxSpeedKph: number;
    isHyperCar: boolean;
    protected engineId: string
    constructor (company: string, model: string, maxSpeed: number, isHyperCar = false, engineId: string) {
        this.company = company;
        this.model = model;
        this.maxSpeedKph = maxSpeed;
        this.isHyperCar = isHyperCar;
        this.engineId = engineId;
    }
    maxSpeedToMph (): string | number {
        return this.maxSpeedKph * .6;
    }
}

class BMW extends Car {
    static vehicle = "Car";
    color: string;
    maxSpeedToMph () {
        return this.maxSpeedKph * .6 + " mph";
    }
    get get_engineId () {
        return this.engineId;
    }
    set set_color (color: string) {
        this.color = color;
    }
}

// vehicle is a static member so to access it we dont need to instanciate BMW class
const vehicle1 = BMW.vehicle;

const i8 = new BMW ("BMW", "i8", 250, true, "fd12");
console.log(i8);
console.log(i8.maxSpeedToMph());
i8.set_color = "white-blue"
console.log(i8);
// we dont have direct access to protected field "engineId"
console.log(i8.get_engineId);

// generic classes - arrow functions
interface Len {
    length: number;
}
class getLen <T extends Len> {
    public a: T;
    constructor (a: T) {
        this.a = a;
    }
    getLen = () => {
        return this.a.length;
    }
}

const arr = new getLen([1, 2, 3]);
console.log(arr.getLen());

// class expressions
const Lamborghini = class extends Car {
    constructor (company: string, model: string, maxSpeed: number, isHyperCar = false, engineId: string) {
        super(company, model, maxSpeed, isHyperCar, engineId);
    }
    getCarName = () => {
        return this.model;
    }
}

const aventador = new Lamborghini("Lamborghini", "Aventador", 350, true, "ad34");
console.log(aventador);
console.log(aventador.maxSpeedToMph());

// abstract class
abstract class Person {
    name: string;
    age: number;
}
// Cannot create an instance of an abstract class
// const person1 = new Person;
class Employee extends Person {
    gender: string;
    constructor (name: string, age: number, gender: string) {
        super();
        this.name = name;
        this.age = age;
        this.gender = gender;
    }
}

const person1 = new Employee("John", 22, "Male");
console.log(person1);

// variable with the type of class "Car" that instanciate class "Lamborghini"
const car1: Car = new Lamborghini("Lamborghini", "Gallardo", 325, true, "hg345");
console.log(car1);
console.log(car1.maxSpeedToMph());

