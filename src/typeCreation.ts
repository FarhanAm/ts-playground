function getId <T> (arg: T): T {
    return arg;
}
// two ways to call a generic function
console.log(getId<string>("waqds"));
console.log(getId(4323));

// generic types
interface GenericGetId <T> {
    (arg: T): T;
}
function getId2 <T> (arg: T): T {
    return arg;
}
let myId: GenericGetId<string> = getId2;
// console.log(myId(234));
console.log(myId("wd32"));

// Generic class
class GenericAdd<T> {
    initVal!: T;
    add!: (x: T, y: T) => T;
}
let myAdd = new GenericAdd<number>();
myAdd.initVal = 2;
myAdd.add = function (a, b) {
    return a + b;
}
console.log(myAdd.add(myAdd.initVal, 10));

// generic constraints
interface Length {
    length: number;
}
function returnLen <T extends Length> (arg: T): T {
    console.log(arg, arg.length);
    return arg;
}
returnLen("Farhan");

// Type parameter in generic constraints
function getEl <T, K extends keyof T> (obj: T, key: K) {
    return obj[key]
};
console.log(getEl({a: 1, b: 2}, "b"));
// console.log(getEl({a: 1, b: 2}, "c"));

// ---------- Keyof Type Opertor
type User = { name: string, age: number };
type Keys = keyof User;
let myObj: Keys = "age";

// ---------- Typeof
function returnNum (n: number) {
    return n;
}
type R = typeof returnNum;

// ---------- Index access
type Car = {model: string, maxSpeed: number, superSport: boolean};
type Torque = Car["maxSpeed"];

type CarType = "superSport" | "maxSpeed";
type C = Car[CarType];

let cars = [
    {model: "Veneno", isSUV: false},
    {model: "Urus", isSUV: false}
];
type C1 = typeof cars[number]["isSUV"];

type K = "model";
type C2 = typeof cars[number][K];

// ---------- conditional typing
type Shape = {
    area: number;
};
interface Circle extends Shape {
    radius: number;
};
interface Square {
    side: number;
}
type shape1 = Circle extends Shape ? number : void;
type shape2 = Square extends Shape ? number : void;

type StringId = {
    strId: string;
}
type NumberId = {
    numId: number;
}
type StrOrNum <T extends number | string> = T extends number ? NumberId : StringId;
let userId: StrOrNum <number> = {numId: 1234};


type ShapeOf <T> = T extends {radius: number} ? T["radius"] : Square;
type CircleShape = ShapeOf<Circle>;

// inferring within conditional types
type ExtractRetrnType <T> = T extends (...args: never[]) => infer Return ? Return : never;
type SquareShape = ExtractRetrnType <(x: 12, y: 2) => Square>;

// distributive conditional types
type ShapesArr <T> = [T] extends [any] ? T[] : never;
type CircleOrSquareArr = ShapesArr<Circle | Square>; // Nice!

// ---------- Mapped Types and Template Literal Types
type MapType <T> = {
    [Property in keyof T] +? : void;
};
type MethodType = {
    changeColor: () => string;
    setAngle: () => number;
}
type MappedMethod = MapType<MethodType>;

type Setter <T> = {
    [Property in keyof T as `SET_${Uppercase<string & Property>}`]: T[Property];
}
interface Person {
    name: string;
    age: number;
    location: string;
};
type SetPerson = Setter<Person>;

type VerifyEmail = "reset_password" | "welcome";
type Emails = `${Capitalize<VerifyEmail>}_Email`;


// ***********************************************
// If you do not export anything from a TypeScript file, 
// it gets compiled in the global scope. To make a .ts file 
// an enclosed module you need to export at least one thing.
export default SetPerson;