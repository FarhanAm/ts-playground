const message = `Hello World!`;
console.log(message);

const color = "Red";
// this will throw an error in both js in runtime and ts
// because color is not callable!
// color();

const car = {
    name: "Corvette",
    color: "black",
};
console.log(car.name);

// this is valid js and return undefined in run time
// but in ts this is an error
// console.log(car.maxSpeed);

// message.toLocaleLowercase();

const a = "a";
const b = "b";
// This condition will always return 'true' since the types '"a"' and '"b"' have no overlap.
// if (a !== "b") {
//     console.log(a);
// } else if (a === "a") {
//     // do sth else
// }

// Explicit Types
function helloUser (name: string, day: string) {
    console.log(`Hello ${name}, today is ${day}`);
}
// helloUser("Farhan", 5);
helloUser("Farhan", "Thursday");

// firstName is a string and we cant assign a number to it
let firstName = "Farhan";
// firstName = 9;
console.log(firstName);

const person = {
    name: "Farhan",
    age: 22
};

const obj1: object = {
    x: 1
}
console.log(obj1);

// the type of a variable is inferred based on the type of its initializer
const obj2 = {
    name: "user1"
}
console.log(typeof(obj2));

// specifying the types of both input and output 
function square (x: number): number {
    return x * x;
}
console.log(square(5));
// logs number
console.log(typeof(square(4)));

// myArr accepts elements of type number
let myArr: number[] = [];
// myArr.push("first");
console.log(myArr);

// optional properties
function multiply (a: number, b?: number) {
    if (b) {
        return a * b;
    }
    return a;
}
console.log(multiply(1));
console.log(multiply(2, 6));

// union types
function getAgeOrName (input: number | string): number | string {
    if (typeof(input) === "string") {
        return(`My name is ${input}`);
    } else {
        return(`I am ${input} years old`);
    }
}
console.log(getAgeOrName("Farzan"));
console.log(getAgeOrName(28));

function printInput (str: string | number): void {
    // handling different inputs
    if (typeof(str) === "string") {
        console.log(str.toUpperCase());
    } else if (typeof(str) === "number") {
        console.log(str + 10);
    }
}
printInput("devEloper");
printInput(23);

//type aliases
type Sides = {
    longitude: number;
    latitud: number;
};
function getRectangleArea (rectangle: Sides) {
    return rectangle.latitud * rectangle.longitude;
}
console.log(getRectangleArea({longitude: 6, latitud: 2}));

type carName = string;
let car1: carName = "Veneno";
console.log(typeof(car1), car1);

// interface
interface Circle {
    radius: number;
}
function circleArea (circle: Circle): number {
    return Math.floor(circle.radius ** 2 * Math.PI);
}
// console.log(circleArea({radius: 3}));
console.log(circleArea({radius: 3, pi: 3}));
// adding new fields to the previous interface
interface Circle {
    radius: number;
    pi: number;
}

const animal = <string>"Tiger";
const animal2 = "Panda" as string;
console.log(typeof(animal), typeof(animal2));

// literal types
let x: "x" = "x";
// x = "y";
// type assertion
x = "z" as "x";
console.log(x);

function getDirection (direction: "left" | "down" | "right" | "up") {
    console.log("go to the " + direction.toUpperCase());
}
getDirection("left");
// getDirection("straight");


// ***********************************************
// If you do not export anything from a TypeScript file, 
// it gets compiled in the global scope. To make a .ts file 
// an enclosed module you need to export at least one thing.
export default getDirection;