// truthiness narrowing
function printNames (name: string | string[]) {
    if (name && typeof(name) === "object") {
        for (let i of name) {
            console.log(i);
        }
    } else if (typeof(name) === "string") {
        console.log(name);
    }
}
printNames(["john", "mike"]);

// equality narrowing
function forFun (x: number | string, y: number | boolean) {
    if (x === y) {
        console.log(Math.pow(x, y));
    } else {
        console.log(x);
        console.log(y);
    }
}
forFun(3, 3);
forFun("true", 3)

interface Cars {
    model: "Aventador" | "Urus" | "Phantom";
}
function carType (cars: Cars) {
    switch (cars.model) {
        case "Aventador":
            return cars.model + " is a Super Sport";
        case "Urus":
            return cars.model + " is a SUV";
        case "Phantom":
            return cars.model + " is a Sedan"
    }
}
// this will return undefined
// console.log(carType({ model: "Veneno" }));
console.log(carType({ model: "Phantom" }));

// instamceof narrowing
type DateType = {
    date: Date | string;
};
function getNewDate (dt: DateType) {
    if (dt.date instanceof Date) {
        console.log(dt.date.getFullYear());
    } else {
        console.log(dt.date.toUpperCase());
    }
}
getNewDate({date: Date()})
getNewDate({date: new Date()})

// assignment
let randNum = Math.floor(Math.random() * 100);
let oddOrEven = randNum % 2 === 0 ? randNum / 2 : "odd";
console.log(oddOrEven);

// type predicates
interface Cat {
    numberOfLives: number
}
interface Dog {
    isAGoodBoy: boolean
}
function isCat (animal: Cat | Dog): animal is Cat {
    return typeof (animal as Cat).numberOfLives === "number";
}
let animal3 = {
    numberOfLives: 9
};
if(isCat(animal3)) {
    console.log("animal3 is a Cat");
} else {
    console.log("animal3 is a Dog");
}

// constrol flow analysis and discriminated unions and exhaustiveness checking
type Circle = {
    kind: "Circle";
    radius: number;
}
interface Square {
    kind: "Square";
    side: number;
}
type Shape = Circle | Square;
function getArea (shape: Shape) {
    switch (shape.kind) {
        case "Circle":
            return shape.radius ** 2 * 3;
        case "Square":
            return shape.side * shape.side;
        default:
            const _exhastiveCheck: never = shape;
            return _exhastiveCheck
    }
}
console.log(getArea({kind: "Circle", radius: 3}));


// ***********************************************
// If you do not export anything from a TypeScript file, 
// it gets compiled in the global scope. To make a .ts file 
// an enclosed module you need to export at least one thing.
export default getArea;