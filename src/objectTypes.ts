interface Student {
    name: string,
    id: string,
    // optional property
    age?: number
}
function getId (student: Student): void {
    console.log("student name: " + student.name);
    console.log("student id: " + student.id);
    student.age && console.log("student age: " + student.age);    
}
getId({name: "Farhan", id: "12qa"});

// properties with default value
interface Pet {
    kind?: string,
    sound?: string
};
function petAction ({kind = "Dog", sound = "woof!"}: Pet) {
    console.log(`${kind} says ${sound}`);
}
petAction({kind: "Cat", sound: "meow!"});
petAction({});

interface SetVal {
    a: number;
    b: number;
}
function setValue (setVal: SetVal) {
    console.log(`a was ${setVal.a}`);
    setVal.a = setVal.b + setVal.a;
    console.log(`a is ${setVal.a}`);
}
setValue({a: 2, b: 3});

// readonly properties
interface SetVal2 {
    readonly a: number;
    b: number;
}
function setValue2 (setVal: SetVal2) {
    console.log(`a was ${setVal.a}`);
    // property a is readonly so we cant reasign it
    // setVal.a = setVal.b + setVal.a;
    console.log(`a is still ${setVal.a}`);
}
setValue2({a: 2, b: 3});

interface PushArr {
    readonly arr: any[];
    pushData: any[]
}
function assign (pushArr: PushArr) {
    console.log(pushArr.arr);
    // ***** property arr is readonly but its internal 
    // ***** contents can be changed
    pushArr.arr.push(...pushArr.pushData);
    console.log(pushArr.arr);
}
assign({arr: [1, 2], pushData: [3]});

// extending types
interface PushArrs extends PushArr {
    pushData2: any[];
}
function assign2 ({arr, pushData, pushData2}: PushArrs) {
    console.log(arr);
    arr.push(...pushData, ...pushData2);
    console.log(arr);    
}
assign2({arr: [1], pushData: [2, 3], pushData2: ["Hello!"]});

// Intersection
interface Car {
    model: string
};
interface Color {
    color: string
};
type CarColor = Car & Color;
const myCar: CarColor = {
    model: "Sian",
    color: "Sunrise Red",
}
console.log(`I hava a(n) ${myCar.color} ${myCar.model}.`);

// Generic Object Types
type Coin = {
    rankOrSymbol: any
};
let coin1: Coin = { rankOrSymbol: "BTC" };
let coin2: Coin = { rankOrSymbol: 1 };
if (typeof(coin1.rankOrSymbol) === "string") {
    console.log(coin2);
}
console.log((coin1.rankOrSymbol as string).toLowerCase());

// Array type
let myArr: Array<number> = [1, 2, 3];
// underneath array definition is similar to the above one
let myArr2: number[] = [1, 2, 3];
// myArr.push(...["a"]);
myArr.push(...[12]);
console.log(myArr);

// tupple types
type Tupple = [string, ...number[],  string];
let myTup: Tupple = ["a", 1, 2, 3, "z"];
console.log(typeof(myTup), myTup);


// ***********************************************
// If you do not export anything from a TypeScript file, 
// it gets compiled in the global scope. To make a .ts file 
// an enclosed module you need to export at least one thing.
export default PushArr;