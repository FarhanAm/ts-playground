// function type expression
function helloUser (fn: (name: string) => void) {
    fn("Farhan");
}
function printMessage (str: string) {
    console.log("hello " + str);
}
helloUser(printMessage);

type functionType = (a: boolean, b: boolean) => boolean;
function logicOr (fn: functionType) {
    return fn(true, false);
}
function orOp (a: boolean, b: boolean) {
    return a || b;
}
console.log(logicOr(orOp));

// Call Signatures
type DescribableFunc = {
    type: string,
    (arg: number): string 
};
function printStr (fn: DescribableFunc) {
    console.log(`this is a ${fn.type.toUpperCase()} and the output is ${fn(6)}`);
}
function numToStr (a: number) {
    return String(a);
}
numToStr.type = "function property";
printStr(numToStr);

// ****************** Construct Signatures ******************
interface ComesFromString {
    name: string;
}

interface StringConstructable {
    new (n: string): ComesFromString;
}

class MadeFromString implements ComesFromString {
    constructor (public name: string) {
        console.log("ctor invoked");
    }
}

function makeObj(n: StringConstructable) {
    return new n("hello!");
}

console.log(makeObj(MadeFromString).name);

// generic functions
function printArrItems <Type> (arr: Type[]) {
    arr.map((item) => console.log(item))
}
printArrItems([2, 3, 5, 7]);

function filter <IN, OUT> (arr: IN[], fn: (arg: IN) => OUT) {
    return arr.filter(fn);
}
const evenNums = filter([1, 2, 3, 4, 5, 6, 7, 8,], (n) => n % 2 === 0);
console.log(evenNums);

// Constraints
function getMaxLen <TYPE extends { length: number }> (a: TYPE, b: TYPE, c: TYPE) {
    let maxLen;
    if (a.length > b.length) {
        maxLen = a;
    } else {
        maxLen = b;
    }
    if (c.length > maxLen.length) {
        maxLen = c;
    }
    return maxLen;
}
// in this function arguments with equal length arent handled properly;
console.log(getMaxLen("a", "asdd", "asdfrwg"));
console.log(getMaxLen([1,2,3], [1], [4]));

////////////////
function filter1 <TYPE> (arr: TYPE[], fn: (arg: TYPE) => boolean): TYPE[] {
    return arr.filter(fn);
}
function filter2 <TYPE, FUNC extends (arg: TYPE) => boolean> (arr: TYPE[], fn: FUNC): TYPE[] {
    return arr.filter(fn);
}

// optional parameter
function multipleTwo (n?: number) {
    if (n) {
        console.log(n * 2);
    } else {
        console.log("multiplier is 2");
    }
}
multipleTwo(2);
multipleTwo();

// function overloads
function add (a: number, b: number): number;
function add (a: number, b: number, c: number): number;
function add (a: string, b: string): string;
function add (a: any, b: any, c?: any) {
    if (c) {
        return a + b + c;
    }
    return a + b;
}
console.log(add(1, 2, 3));
console.log(add("farhan", " aqaei"));

// this key word in function
const greetUser = {
    name: "Admin",
    greet() {
        console.log("Hello " + this.name);
    }
}
greetUser.greet();

// rest parameters
function addParams (...params: number[]) {
    let result = 0;
    for (let i of params) {
        result += i;
    }
    return result;
}
console.log(addParams(2, 4, 5));

// rest arguments
let a1 = [1, 3, 4];
let a2 = [5, 6];
a1.push(...a2);
console.log(a1);


// ***********************************************
// If you do not export anything from a TypeScript file, 
// it gets compiled in the global scope. To make a .ts file 
// an enclosed module you need to export at least one thing.
export default addParams;