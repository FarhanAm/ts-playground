import add, { factorial, power } from "./math";

const arr = [1, 2, 3, 4, 5];
console.log(add(...arr));

console.log(factorial(5));

console.log(power(2, 10));
