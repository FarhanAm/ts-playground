function add (...args: number[]) {
    let result = 0;
    args.forEach((o) => result += o);
    return result;
}

function factorial (n: number): number {
    if (n === 1) {
        return 1;
    }
    return n * factorial(n - 1);
}

function power (x: number, y: number): number {
    if (y === 1) {
        return x;
    }
    return x * power(x, y - 1);
}

export default add;
export { power, factorial };